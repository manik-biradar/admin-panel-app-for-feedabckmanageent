import { Component, OnInit , Inject} from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})
export class AdduserComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public snackBar: MatSnackBar) { }


    openSnackBar() {
      this.snackBar.open('Record Changed', 'Undo', {
		  duration: 3000
		});
    }

  ngOnInit() {
  }

}
