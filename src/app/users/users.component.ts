import { Component, OnInit } from '@angular/core';
import { AdduserComponent } from './adduser/adduser.component';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  constructor(public dialog: MatDialog) {}

    openEdit() {
      this.dialog.open(AdduserComponent, {
        data: {
          animal: 'panda'
        }
      });
    }

  ngOnInit() {
  }


}
