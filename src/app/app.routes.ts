import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { BlogComponent } from './blog/blog.component';
import { ViewComponent } from './view/view.component';
import { ReportComponent } from './report/report.component';

export const ROUTES: Routes = [
  	{ path: '', component: AppComponent },
  	{ path: 'users', component: UsersComponent },
  	{ path: 'view', component: ViewComponent},
  	{ path: 'blog', component: BlogComponent},  
	{ path: 'report', component: ReportComponent}

  

];