import { Component, OnInit } from '@angular/core';
import { Chart } from 'angular-highcharts';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  pieChart = new Chart({
  	chart: {
  	        plotBackgroundColor: null,
  	        plotBorderWidth: null,
  	        plotShadow: false,
  	        type: 'pie'
  	    },
  	    title: {
  	        text: 'Browser market shares in January, 2018'
  	    },
  	    tooltip: {
  	        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  	    },
  	    credits: {
  	          enabled: false
  	    }, 
  	    plotOptions: {
  	        pie: {
  	            allowPointSelect: true,
  	            cursor: 'pointer',
  	            dataLabels: {
  	                enabled: false,
  	                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
  	                style: {
  	                    //	color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
  	                }
  	            }
  	        }
  	    },
  	    series: [{
  	        name: 'Brands',
  	        //colorByPoint: true,
  	        data: [{
  	            name: 'Chrome',
  	            y: 61.41,
  	            /*sliced: true,
  	            selected: true*/
  	        }, {
  	            name: 'Internet Explorer',
  	            y: 11.84
  	        }, {
  	            name: 'Firefox',
  	            y: 10.85
  	        }, {
  	            name: 'Edge',
  	            y: 4.67
  	        }, {
  	            name: 'Safari',
  	            y: 4.18
  	        }, {
  	            name: 'Sogou Explorer',
  	            y: 1.64
  	        }, {
  	            name: 'Opera',
  	            y: 1.6
  	        }, {
  	            name: 'QQ',
  	            y: 1.2
  	        }, {
  	            name: 'Other',
  	            y: 2.61
  	        }]
  	    }]
  })

  barChart = new Chart({
          chart: {
              type: 'bar'
          },
          title: {
              text: 'Monthly Average User'
          },
          /*subtitle: {
              text: 'Source: WorldClimate.com'
          },*/
          xAxis: {
              categories: [
                  'Jan',
                  'Feb',
                  'Mar',
                  'Apr',
                  'May',
                  'Jun',
              ]
          },
          yAxis: {
              min: 0,
              title: {
                  text: 'Users'
              }
          },
          credits: {
                enabled: false
            },   
        legend: {
        		enabled: false,
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -40,
                y: 80,
                floating: true,
                borderWidth: 1,
               // backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                shadow: true
            },       
          plotOptions: {

              column: {              		
                  pointPadding: 0.2,
                  borderWidth: 0
              },
          },
          series: [{
              name: 'User',
              data: [5,6,2,8,9,5]

          }],
      });
}
